const readline = require('readline');
const os = require('os');
const fs = require('fs');
const { PORT = 3000 } = process;

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
  });
  rl.question('Choose an option: \n1. Read package.json\n2. Display OS information`\n3. Start HTTP server\nType a number: ',
   (answer) => {
       if (answer == 1){
        fs.readFile('package.json', 'utf-8', (error, fileContent)=> {
            console.log(fileContent);
          });
       } else if (answer == 2){
           console.log('Getting OS info...');
           console.log('SYSTEM MEMORY: ' + (os.totalmem()/1024/1024/1024).toFixed(2) + 'GB');
           console.log('FREE MEMORY: ' + (os.freemem()/1024/1024/1024).toFixed(2) + 'GB');
           console.log('CPU CORES: ' + os.cpus().length + ' cores');
           console.log('ARCH: ' + os.arch());
           console.log('PLATFORM: ' + os.platform());
           console.log('RELEASE: ' + os.release());
           console.log('USER: ' + os.userInfo().username);
       } else if (answer == 3){
           const http = require('http').createServer(async function(req, res){
               res.setHeader('Content-type', 'text/html')
               try {
                   res.write("Hello World");
                   return res.end();
                }   catch (e){
                    return res.end(e.message);
                } 
            });
            http.listen(PORT, () => {
                console.log("Server running on port " + PORT)
            });
       } else {
           console.log("Not a valid option! Exiting program.");
       }  
    rl.close();
  });
  