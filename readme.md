# Console APp

Assignment 2/3 for Experis Academy's JavaScript course. A node.js console-app that displays info about package.json, the computer system or starts a Hello World-server.

## Getting Started

### Prerequesites

* Node.js 

### Run the application
Open the computer's console/terminal and run this command:
> node index.js

## Built With

* Visual Studio Code
* Node.js

## Author

* Charlotte Ødegården

## License

Still don't know enough about licenses to firmly state anything about this. 